package net.thumbtack.school.elections;

import com.google.gson.Gson;
import net.thumbtack.school.elections.db.DataBase;
import net.thumbtack.school.elections.exeption.ElectionsExeption;
import net.thumbtack.school.elections.model.UserModel;
import net.thumbtack.school.elections.server.Server;
import net.thumbtack.school.elections.service.ElectionService;
import net.thumbtack.school.elections.service.RegisterService;
import net.thumbtack.school.elections.service.SentenceService;
import net.thumbtack.school.elections.service.Service;
import org.junit.Test;

import java.io.IOException;

public class TestServer {

    // для выдвижения кандидата используется 3 параметра,
    // 0 - никто не выдвигает кандидата
    // 1 - кто то выдвинул кандидата, ждёт подтверждения
    // 2 - кандидат подтвердил выдвижение
    // пока что автоматом подтверждает

    @Test
    public void testRegisterVoter() throws IOException, ElectionsExeption {
        Gson gson = new Gson();
        DataBase dataBase = DataBase.getInstance();
        UserModel userModel = new UserModel();
        UserModel userModel2 = new UserModel();

        String json = "{ \"firstname\": \"sergey\", \"midlname\": \"pavlovich\", \"lastname\": \"garbar\", \"street\": \"street1\", \"house\": 32, \"apartment\": 32, " +
                "\"login\": \"user3\", \"password\": \"password1\", \"voiter\": true, \"candidate\": false}";
        String json2 = "{ \"firstname\": \"Ivan\", \"midlname\": \"pavlovich\", \"lastname\": \"ivanov\", \"street\": \"street1\", \"house\": 32, \"apartment\": 32," +
                " \"login\": \"snik2004\", \"password\": \"password2\", \"voiter\": true, \"candidate\": false, \"nomination\": 0}";
        String json3 = "{\"firstname\":\"Ivan\",\"midlname\":\"pavlovich\",\"lastname\":\"ivanov\",\"street\":\"street1\",\"house\":32,\"apartment\":32," +
                "\"login\":\"user8\",\"password\":\"password\",\"voiter\":true,\"candidate\":false, \"nomination\": 1}";
        String json4 = "{ \"login\": \"user10\", \"password\": \"password1\", \"voiter\": false, \"candidate\": true, " +
                "\"firstname\": \"ivan\", \"midlname\": \"ivanovich\", \"lastname\": \"ivanov\", \"street\": \"street1\", \"house\": 3223, \"apartment\": 322 }";
        String json5 = "{ \"login\": \"user007\", \"password\": \"password1\", \"voiter\": false, \"candidate\": true," +
                " \"firstname\": \"petr\", \"midlname\": \"petrovich\", \"lastname\": \"petrov\", \"street\": \"street1\", \"house\": 3, \"apartment\": 234 }";
        String json6 = "{ \"login\": \"user008\", \"password\": \"password1\", \"voiter\": true, \"candidate\": false," +
                " \"firstname\": \"petr\", \"midlname\": \"petrovich\", \"lastname\": \"petrov\", \"street\": \"street1\", \"house\": 3, \"apartment\": 234 }";
        //
//        System.out.println(json);
//        System.out.println(json2);
        String login = "{\"login\":\"user3\",\"password\":\"password1\"}";
        String login2 = "{\"login\": \"snik2004\", \"password\": \"password2\"}";
        String login3 = "{\"login\": \"user8\", \"password\": \"password\"}";
        String login4 = "{\"login\": \"user007\", \"password\": \"password1\"}";
        String login5 = "{\"login\": \"user10\", \"password\": \"password1\"}";
        String login6 = "{\"login\": \"user008\", \"password\": \"password1\"}";

        //тест кандидатов
        String testCandidate = "{\"login\":\"user3\"}";
        String testCandidate2 = "{\"login\": \"snik2004\"}";
        String testCandidate3 = "{\"login\": \"user8\"}";
        String testCandidate4 = "{\"login\": \"user10\"}";
        String testCandidate5 = "{\"login\": \"user007\"}";

        //json голосов
        String jsonVote = "{\"voteUsername\": \"user8\", \"candidateUsername\": \"user10\"}";
        //String jsonVote2 = "{\"voteUsername\": \"user3\", \"candidateUsername\": \"против всех\"}";
        String jsonVote2 = "{\"voteUsername\": \"user3\", \"candidateUsername\": \"snik2004\"}";
        String jsonVote3 = "{\"voteUsername\": \"user8\", \"candidateUsername\": \"user8\"}";
        String jsonVote4 = "{\"voteUsername\": \"user8\",\"candidateUsername\": \"против всех\"}";
        String jsonVote5 = "{\"voteUsername\": \"snik2004\",\"candidateUsername\": \"user3\"}";
        String jsonVote6 = "{\"voteUsername\": \"user10\",\"candidateUsername\": \"user3\"}";
        String jsonVote7 = "{\"voteUsername\": \"user007\",\"candidateUsername\": \"user10\"}";
        String jsonVote8 = "{\"voteUsername\": \"snik2004\",\"candidateUsername\": \"против всех\"}";
        String jsonVote9 = "{\"voteUsername\": \"user008\",\"candidateUsername\": \"snik2004\"}";

        //json предлодения
        String jsonSentence1 = "{\"autor\":\"snik2004\",\"sentence\":\"поставить ёлку что бы без соли\"}";
        String jsonSentence2 = "{\"autor\":\"user007\",\"sentence\":\"предложение 1\"}";
        String jsonSentence3 = "{\"autor\":\"user8\",\"sentence\":\"предложение 2\"}";
        String jsonSentence4 = "{\"autor\":\"user10\",\"sentence\":\"предложение 3\"}";
        String jsonSentence5 = "{\"autor\":\"user3\",\"sentence\":\"предложение 4\"}";
        String jsonSentence6 = "{\"autor\":\"user3\",\"sentence\":\"предложение 4\"}";
        String jsonSentence7 = "{\"autor\":\"user8\",\"sentence\":\"поставить ёлку что бы без соли\"}";
        String jsonSentence8 = "{\"autor\":\"user007\",\"sentence\":\"предложение 5\"}";

        //json оценки предложения
        String jsonRate1 = "{\"user\":\"snik2004\",\"sentence\":\"поставить ёлку что бы без соли\",\"rate\":5}";
        String jsonRate2 = "{\"user\":\"user007\",\"sentence\":\"предложение 1\",\"rate\":4}";
        String jsonRate3 = "{\"user\":\"user8\",\"sentence\":\"предложение 2\",\"rate\":2}";
        String jsonRate4 = "{\"user\":\"user10\",\"sentence\":\"предложение 3\",\"rate\":3}";
        String jsonRate5 = "{\"user\":\"user3\",\"sentence\":\"предложение 4\",\"rate\":4}";
        String jsonRate6 = "{\"user\":\"user3\",\"sentence\":\"предложение 4\",\"rate\":1}";

        //json просмотра предложений определённого кандидата
        String jsonShowSentence = "{\"candidate\":\"user3\",\"sentence\":\"предложение 4\"}";
        String jsonShowAllSentence = "{\"candidate\":\"user3\",\"sentence\":\"all\"}";
        String jsonShowAllSentence2 = "{\"candidate\":\"user8\",\"sentence\":\"all\"}";

        //json добавления предложений к программе кандидата
        String jsonAddSentence1="{\"candidate\":\"user3\",\"sentence\":\"предложение 4\"}";
        String jsonAddSentence2="{\"candidate\":\"user3\",\"sentence\":\"предложение 1\"}";
        String jsonAddSentence3="{\"candidate\":\"user3\",\"sentence\":\"предложение 2\"}";
        String jsonAddSentence4="{\"candidate\":\"user3\",\"sentence\":\"предложение 4\"}";
        String jsonAddSentence5="{\"candidate\":\"user8\",\"sentence\":\"предложение 5\"}";
        String jsonAddSentence6="{\"candidate\":\"user8\",\"sentence\":\"предложение 4\"}";
        String jsonAddSentence7="{\"candidate\":\"user8\",\"sentence\":\"предложение 8\"}";


//        System.out.println(jsonShowSentence);
//        System.out.println(jsonShowAllSentence);


//         вывести предложения в консоль
//        sentenceService.showAllSentence();


        Server server = new Server();
        Service service = new Service();
        server.startServer("1.txt");



        //регистрация
        RegisterService.registerUser(json);
        RegisterService.registerUser(json2);
        RegisterService.registerUser(json3);
        RegisterService.registerUser(json4);
        RegisterService.registerUser(json5);
        RegisterService.registerUser(json6);

        //логин логаут
        String token = server.logIn(login);
        server.logOut(token);

        server.logIn(login);
        server.logIn(login2);
        server.logIn(login3);
        server.logIn(login4);
        server.logIn(login5);
        server.logIn(login6);

        token = server.logIn(login4);
        server.logOut(token);

        //кандидаты
        ElectionService electionService = new ElectionService();
        electionService.setCandidate(testCandidate);
        electionService.setCandidate(testCandidate3);
        electionService.setCandidate(testCandidate2);
        electionService.checkNomination(testCandidate);
        electionService.checkNomination(testCandidate2);
        electionService.checkNomination(testCandidate3);
        electionService.checkNomination(testCandidate4);
        electionService.checkNomination(testCandidate5);
        electionService.noCandidate(testCandidate3);
        electionService.checkStatusCandidate();
        service.candidateUser();
        service.onlineUser();


        //добавление предлолжений
        SentenceService sentenceService = new SentenceService();
        sentenceService.addSentence(jsonSentence1);
        sentenceService.addSentence(jsonSentence2);
        sentenceService.addSentence(jsonSentence3);
        sentenceService.addSentence(jsonSentence4);
        sentenceService.addSentence(jsonSentence5);
        sentenceService.addSentence(jsonSentence6);
        sentenceService.addSentence(jsonSentence7);
        sentenceService.addSentence(jsonSentence8);

        //голосование за предлодения
        sentenceService.addRate(jsonRate1);
        sentenceService.addRate(jsonRate2);
        sentenceService.addRate(jsonRate3);
        sentenceService.addRate(jsonRate4);
        sentenceService.addRate(jsonRate5);
        sentenceService.addRate(jsonRate6);

        sentenceService.showAllSentence();
        sentenceService.showSentence();

        //добавления предложений в программу кандидата
        sentenceService.addSentenceCandidate(jsonAddSentence1);
        sentenceService.addSentenceCandidate(jsonAddSentence2);
        sentenceService.addSentenceCandidate(jsonAddSentence3);
        sentenceService.addSentenceCandidate(jsonAddSentence4);
        sentenceService.addSentenceCandidate(jsonAddSentence5);
        sentenceService.addSentenceCandidate(jsonAddSentence6);
        sentenceService.addSentenceCandidate(jsonAddSentence7);

        sentenceService.searchSentence(jsonShowSentence);
        sentenceService.searchSentence(jsonShowAllSentence);
        sentenceService.searchSentence(jsonShowAllSentence2);

        //голосование
        electionService.voting(jsonVote);
        electionService.voting(jsonVote2);
        electionService.voting(jsonVote3);
        electionService.voting(jsonVote4);
        electionService.voting(jsonVote5);
        electionService.voting(jsonVote6);
        electionService.voting(jsonVote7);
        electionService.voting(jsonVote8);
        electionService.voting(jsonVote9);
        electionService.voteListView();

        //server.stopServer("1.txt");
    }

}
