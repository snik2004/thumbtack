package net.thumbtack.school.elections.db;

import net.thumbtack.school.elections.model.ProgramModel;
import net.thumbtack.school.elections.model.SentenceModel;
import net.thumbtack.school.elections.model.UserModel;
import net.thumbtack.school.elections.model.VoteModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class DataBase {
    private static DataBase instance;

    private DataBase() {

    }

    public static synchronized DataBase getInstance() {
        if (instance == null) {
            instance = new DataBase();
        }
        return instance;
    }

    private Map<String, UserModel> usersDB = new HashMap();
    private Map<String, VoteModel> voteDB = new HashMap();
    private List sentenceDB = new ArrayList<String>();
    private Map<Integer, SentenceModel> sentenceRateBD = new HashMap();
    private Map<String, ProgramModel> programDB = new HashMap();
    private int countAagainstAll = 0;

    //добавление нового юзера
    public void addUser(UserModel userModel) {
        usersDB.put(userModel.getLogin(), userModel);
    }


    //добавление нового предложения
    public void addSentence(SentenceModel sentenceModel) {
        sentenceRateBD.put(sentenceDB.size(), sentenceModel);
        sentenceDB.add(sentenceModel.getSentence());
    }

    public void addAgainstVote() {
        countAagainstAll++;
    }
    public int returnCountAgianst(){
        return countAagainstAll;
    }
    //добавление предложения в программу
    public void addProgramSentence(ProgramModel programModel) {
        programDB.put(programModel.getCandidate(), programModel);
    }

    //добавление оценки предложения
    public void addRateSentence(SentenceModel sentenceModel) {
        sentenceRateBD.put(sentenceModel.getId(), sentenceModel);
    }

    //добавление нового голоса
    public void addVote(VoteModel voteModel) {
        voteDB.put(voteModel.getCandidateUsename(), voteModel);
    }

    //авторизация юзера
    public void loginUser(UserModel userModel) {
        usersDB.put(userModel.getLogin(), userModel);
    }

    // передача базы пользователей
    public Map<String, UserModel> returnBaseUser() {
        return usersDB;
    }

    // передача базы предложений
    public List returnBaseSentence() {
        return sentenceDB;
    }

    // передача базы программ
    public Map<String, ProgramModel> returnBaseProgram() {
        return programDB;
    }

    // передача базы оценок предложений
    public Map<Integer, SentenceModel> returnBaseRateSentence() {
        return sentenceRateBD;
    }

    // передача базы голосов
    public Map<String, VoteModel> returnBaseVotes() {
        return voteDB;
    }

    //удаление кандидата из базы
    public void deleteCandidate(String s) {
        voteDB.remove(s);
    }


    //
    public String getUserInfo(String token) {
        System.out.print(usersDB.get(token) + "\n");
        return String.valueOf(usersDB.get(token));
    }

}
