package net.thumbtack.school.elections.exeption;

public enum  ElectionsErrorCode {
    ELECTIONS_WRONG_FIRSTNAME("incorrect firstname"),
    ELECTIONS_WRONG_LASTNAME("incorrect lastname"),
    ELECTIONS_WRONG_APARTMENT("incorrect apartment"),
    ELECTIONS_WRONG_STREET("incorrect street"),
    ELECTIONS_WRONG_LOGIN("incorrect login"),
    ELECTIONS_WRONG_HOUSE("incorrect house"),
    ELECTIONS_WRONG_PASSWORD("incorrect pasword"),
    ERROR_FILE("no file");

    private String errorMessage;

    public String getErrorMessage() {
        return errorMessage;
    }

    private ElectionsErrorCode (String errorMessage){
        this.errorMessage=errorMessage;

    }

}
