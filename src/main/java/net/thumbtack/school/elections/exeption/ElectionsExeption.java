package net.thumbtack.school.elections.exeption;

public class ElectionsExeption extends Throwable {
    public ElectionsExeption(ElectionsErrorCode errorCode) {
        this.errorCode=errorCode;
    }

    public ElectionsErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ElectionsErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    ElectionsErrorCode errorCode;

}
