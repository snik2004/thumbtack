package net.thumbtack.school.elections.model;

import java.util.Objects;

public class UserModel {
    private String token;
    private String login;
    private String password;
    private Boolean voiter;
    private Boolean candidate;
    private String firstname;
    private String midlname;
    private String lastname;
    private int nomination;
    private int voted;
    private int id;

    private String vote;
    private String street;
    private Integer house;
    private Integer apartment;

    public int getVoted() {
        return voted;
    }

    public void setVoted(int voted) {
        this.voted = voted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserModel userModel = (UserModel) o;
        return Objects.equals(token, userModel.token) &&
                Objects.equals(login, userModel.login) &&
                Objects.equals(password, userModel.password) &&
                Objects.equals(voiter, userModel.voiter) &&
                Objects.equals(candidate, userModel.candidate) &&
                Objects.equals(firstname, userModel.firstname) &&
                Objects.equals(midlname, userModel.midlname) &&
                Objects.equals(lastname, userModel.lastname) &&
                Objects.equals(street, userModel.street) &&
                Objects.equals(house, userModel.house) &&
                Objects.equals(apartment, userModel.apartment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(token, login, password, voiter, candidate, firstname, midlname, lastname, street, house, apartment);
    }



    public int getNomination() {
        return nomination;
    }

    public void setNomination(int nomination) {
        this.nomination = nomination;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getVoiter() {
        return voiter;
    }

    public void setVoiter(Boolean voiter) {
        this.voiter = voiter;
    }

    public Boolean getCandidate() {
        return candidate;
    }

    public void setCandidate(Boolean candidate) {
        this.candidate = candidate;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMidlname() {
        return midlname;
    }

    public void setMidlname(String midlname) {
        this.midlname = midlname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouse() {
        return house;
    }

    public void setHouse(Integer house) {
        this.house = house;
    }

    public Integer getApartment() {
        return apartment;
    }

    public void setApartment(Integer apartment) {
        this.apartment = apartment;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }
}
