package net.thumbtack.school.elections.model;

import java.util.ArrayList;
import java.util.List;

public class SentenceModel {
    private int id;

    private String sentence;
    private int rateMidle;
    private List<RateModel> rateModel = new ArrayList();
    private String autor;

    public String getSentence() {
        return sentence;
    }

    public int getId() {
        return id;
    }

    public int getRateMidle() {
        return rateMidle;
    }

    public void setRateMidle(int rateMidle) {
        this.rateMidle = rateMidle;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public List<RateModel> getRateModel() {
        return rateModel;
    }

    public void setRateModel(List<RateModel> rateModel) {
        this.rateModel = rateModel;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }


}
