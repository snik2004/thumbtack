package net.thumbtack.school.elections.model;

import java.util.ArrayList;
import java.util.List;

public class ProgramModel {
    private String candidate;
    private int id;
    private List<SentenceModel> sentenceModelList = new ArrayList();

    public List<SentenceModel> getSentenceModelList() {
        return sentenceModelList;
    }

    public void setSentenceModelList(List<SentenceModel> sentenceModelList) {
        this.sentenceModelList = sentenceModelList;
    }



    public String getCandidate() {
        return candidate;
    }

    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
