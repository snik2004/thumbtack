package net.thumbtack.school.elections.model;

public class VoteModel {

    private String token;
    private String candidateUsename;
    private Integer countvote;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCandidateUsename() {
        return candidateUsename;
    }

    public void setCandidateUsename(String candidateUsename) {
        this.candidateUsename = candidateUsename;
    }

    public Integer getCountvote() {
        return countvote;
    }

    public void setCountvote(Integer countvote) {
        this.countvote = countvote;
    }
}
