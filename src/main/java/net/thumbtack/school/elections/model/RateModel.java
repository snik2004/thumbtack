package net.thumbtack.school.elections.model;

public class RateModel {
    String rateAutor;
    int sentencePosition;
    String idCandidateProgram;
    int rate;

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }



    public String getRateAutor() {
        return rateAutor;
    }

    public void setRateAutor(String rateAutor) {
        this.rateAutor = rateAutor;
    }

    public int getSentencePosition() {
        return sentencePosition;
    }

    public void setSentencePosition(int sentencePosition) {
        this.sentencePosition = sentencePosition;
    }

    public String getIdCandidateProgram() {
        return idCandidateProgram;
    }

    public void setIdCandidateProgram(String idCandidateProgram) {
        this.idCandidateProgram = idCandidateProgram;
    }
}
