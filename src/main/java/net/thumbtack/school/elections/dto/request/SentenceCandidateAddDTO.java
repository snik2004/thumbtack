package net.thumbtack.school.elections.dto.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SentenceCandidateAddDTO {
    @SerializedName("candidate")
    @Expose
    private String candidate;

    @SerializedName("sentence")
    @Expose
    private String sentence;

    public String getCandidate() {
        return candidate;
    }

    public void setCandidate(String candidate) {
        this.candidate = candidate;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

}
