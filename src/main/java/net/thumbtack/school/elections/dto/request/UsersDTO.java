package net.thumbtack.school.elections.dto.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class UsersDTO {

    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("midlname")
    @Expose
    private String midlname;
    @SerializedName("lastname")
    @Expose
    private String lastname;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UsersDTO usersDTO = (UsersDTO) o;
        return Objects.equals(firstname, usersDTO.firstname) &&
                Objects.equals(midlname, usersDTO.midlname) &&
                Objects.equals(lastname, usersDTO.lastname) &&
                Objects.equals(street, usersDTO.street) &&
                Objects.equals(house, usersDTO.house) &&
                Objects.equals(apartment, usersDTO.apartment) &&
                Objects.equals(login, usersDTO.login) &&
                Objects.equals(password, usersDTO.password) &&
                Objects.equals(voiter, usersDTO.voiter) &&
                Objects.equals(candidate, usersDTO.candidate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstname, midlname, lastname, street, house, apartment, login, password, voiter, candidate);
    }

    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("house")
    @Expose
    private Integer house;
    @SerializedName("apartment")
    @Expose
    private Integer apartment;
    @SerializedName("login")
    @Expose
    private String login;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("voiter")
    @Expose
    private Boolean voiter;
    @SerializedName("candidate")
    @Expose
    private Boolean candidate;

    public int getNomination() {
        return nomination;
    }

    public void setNomination(int nomination) {
        this.nomination = nomination;
    }

    @SerializedName("nomination")
    @Expose
    private int nomination;

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMidlname() {
        return midlname;
    }

    public void setMidlname(String midlname) {
        this.midlname = midlname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getHouse() {
        return house;
    }

    public void setHouse(Integer house) {
        this.house = house;
    }

    public Integer getApartment() {
        return apartment;
    }

    public void setApartment(Integer apartment) {
        this.apartment = apartment;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Boolean getVoiter() {
        return voiter;
    }

    public void setVoiter(Boolean voiter) {
        this.voiter = voiter;
    }

    public Boolean getCandidate() {
        return candidate;
    }

    public void setCandidate(Boolean candidate) {
        this.candidate = candidate;
    }


}