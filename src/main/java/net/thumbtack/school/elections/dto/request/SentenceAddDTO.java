package net.thumbtack.school.elections.dto.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SentenceAddDTO {
    @SerializedName("sentence")
    @Expose
    private String sentence;

    @SerializedName("autor")
    @Expose
    private String autor;

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }


    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

}
