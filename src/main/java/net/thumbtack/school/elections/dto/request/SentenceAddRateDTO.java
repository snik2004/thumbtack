package net.thumbtack.school.elections.dto.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SentenceAddRateDTO {
    @SerializedName("rate")
    @Expose
    private Integer rate;
    @SerializedName("sentence")
    @Expose
    private String sentence;
    @SerializedName("user")
    @Expose
    private String user;

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getSentence() {
        return sentence;
    }

    public void setSentence(String sentence) {
        this.sentence = sentence;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }
}
