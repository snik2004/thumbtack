package net.thumbtack.school.elections.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseErrorDTO {
    @SerializedName("error")
    @Expose
    private String error;
    @SerializedName("errormessage")
    @Expose
    private String errormessage;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrormessage() {
        return errormessage;
    }

    public void setErrormessage(String errormessage) {
        this.errormessage = errormessage;
    }
}
