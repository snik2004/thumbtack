package net.thumbtack.school.elections.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseElectionDTO {
    @SerializedName("responseStatusCandidate")
    @Expose
    private String responseStatusCandidate;
    @SerializedName("candidateUsername")
    @Expose
    private String candidateUsername;
    @SerializedName("responseMesage")
    @Expose
    private String responseMesage;

    public String getResponseStatusCandidate() {
        return responseStatusCandidate;
    }

    public void setResponseStatusCandidate(String responseStatusCandidate) {
        this.responseStatusCandidate = responseStatusCandidate;
    }

    public String getCandidateUsername() {
        return candidateUsername;
    }

    public void setCandidateUsername(String candidateUsername) {
        this.candidateUsername = candidateUsername;
    }

    public String getResponseMesage() {
        return responseMesage;
    }

    public void setResponseMesage(String responseMesage) {
        this.responseMesage = responseMesage;
    }

}
