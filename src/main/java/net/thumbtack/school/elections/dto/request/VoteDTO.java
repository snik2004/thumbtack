package net.thumbtack.school.elections.dto.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoteDTO {

    @SerializedName("candidateUsername")
    @Expose
    private String candidateUsername;
    @SerializedName("voteUsername")
    @Expose
    private String voteUsername;
    @SerializedName("countvote")
    @Expose
    private Integer countvote;

    public String getCandidateUsername() {
        return candidateUsername;
    }

    public void setCandidateUsername(String candidateUsername) {
        this.candidateUsername = candidateUsername;
    }

    public String getVoteUsername() {
        return voteUsername;
    }

    public void setVoteUsername(String voteUsername) {
        this.voteUsername = voteUsername;
    }

    public Integer getCountvote() {
        return countvote;
    }

    public void setCountvote(Integer countvote) {
        this.countvote = countvote;
    }

}
