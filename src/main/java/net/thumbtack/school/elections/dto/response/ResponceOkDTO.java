package net.thumbtack.school.elections.dto.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponceOkDTO {
    public String getOk() {
        return ok;
    }

    public void setOk(String ok) {
        this.ok = ok;
    }

    @SerializedName("ok")
    @Expose
    private String ok;
}
