package net.thumbtack.school.elections;

import com.google.gson.Gson;
import net.thumbtack.school.elections.db.DataBase;
import net.thumbtack.school.elections.dto.request.LoginDTO;
import net.thumbtack.school.elections.dto.request.UsersDTO;
import net.thumbtack.school.elections.dto.response.ResponseErrorLoginDTO;
import net.thumbtack.school.elections.exeption.ElectionsErrorCode;
import net.thumbtack.school.elections.exeption.ElectionsExeption;
import net.thumbtack.school.elections.model.UserModel;
import net.thumbtack.school.elections.service.Service;

import java.util.HashMap;
import java.util.Map;

public class CheckUser {
    public static String checkUserJSON(UsersDTO userDTO) throws ElectionsExeption {
        String returnString = "ok";
        ResponseErrorLoginDTO responseErrorLoginDTO = new ResponseErrorLoginDTO();
        Gson gson = new Gson();
        Map<String, UsersDTO> checkUserDataBase = new HashMap<>();
        DataBase dataBase = DataBase.getInstance();
        if (userDTO.getLogin().equals("против всех")){
            returnString="ok";
            return returnString;
        }
        if (userDTO == null) System.out.println("Error");
        assert userDTO != null;
        if (userDTO.getLogin().equals("") || userDTO.getLogin() == null) {
            UsersDTO userDTOSearchLogin = new UsersDTO();
            for (Map.Entry entry : checkUserDataBase.entrySet()) {
                userDTOSearchLogin = (UsersDTO) entry.getValue();
                if (!userDTO.getLogin().equals(userDTOSearchLogin.getLogin())) {
                    System.out.println("Пользователь " + userDTO.getLogin() + " не найден");
                    responseErrorLoginDTO.setError("Ппользователь не найден");
                    returnString = gson.toJson(responseErrorLoginDTO, ResponseErrorLoginDTO.class);
                    return returnString;
                }
            }
            returnString = "Error login";
            throw new ElectionsExeption(ElectionsErrorCode.ELECTIONS_WRONG_LOGIN);
        }
        try {
            if (userDTO.getPassword() == null || userDTO.getPassword().length() < 8) {
                returnString = "Error password";
                System.out.println(userDTO.getLogin() + ": Пароль не удовлетворяет условиям");
                responseErrorLoginDTO.setLogin(userDTO.getLogin());
                responseErrorLoginDTO.setPassword(userDTO.getPassword());
                responseErrorLoginDTO.setError("Пароль не удовлетворяет условиям");
                returnString = gson.toJson(responseErrorLoginDTO, ResponseErrorLoginDTO.class);
                //System.out.println(returnString);
                return returnString;
               // throw new ElectionsExeption(ElectionsErrorCode.ELECTIONS_WRONG_PASSWORD);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (userDTO.getFirstname().equals("") || userDTO.getFirstname() == null) {
            returnString = "Error firstname";
            throw new ElectionsExeption(ElectionsErrorCode.ELECTIONS_WRONG_FIRSTNAME);

        }
        try {
            if (userDTO.getLastname() == "" || userDTO.getLastname() == null) {
                returnString = "Error lastname";
                throw new ElectionsExeption(ElectionsErrorCode.ELECTIONS_WRONG_LASTNAME);

            }
        } catch (ElectionsExeption exeption) {
            exeption.setErrorCode(ElectionsErrorCode.ELECTIONS_WRONG_LASTNAME);
        }
        if (userDTO.getApartment() == null) {
            System.out.println("Error apartment");
            returnString = "Error apartment";
        }
        if (userDTO.getHouse() == null) {
            System.out.println("Error house");
            returnString = "Error house";
        }
        if (userDTO.getStreet() == null || userDTO.getStreet().equals("")) {
            System.out.println("Error street");
            returnString = "Error street";
        }
        if (!userDTO.getVoiter() && !userDTO.getCandidate()) {
            System.out.println("Error человек не избиратель и не кандидат");
            returnString = "Error человек не избиратель и не кандидат";
        }
//        returnString = gson.toJson(responseErrorLoginDTO, ResponseErrorLoginDTO.class);
//        System.out.println(returnString);
        return returnString;
    }

    //проверка есть ли пользователь в базе по логину
    public static String checkAddUserLogin(String login) {
        Service service = new Service();
        Gson gson = new Gson();
        //UserModel userModel = gson.fromJson(login,UserModel.class);
//        String response = userModel.getCandidateUsername();
        if (service.searchUserByLogin(login) != null) {
            //System.out.println("Пользователь уже существует");
            return "userexist";
        }
        return "ok";
    }
//    //проверка есть ли пользователь в базе по логину при авторизации
//    public static String checkLoginUser(String login) {
//        Service service = new Service();
//        Gson gson = new Gson();
//        UserModel userModel = gson.fromJson(login,UserModel.class);
////        String response = userModel.getCandidateUsername();
//        if (service.searchUserByLogin(login) != null) {
//            return "Пользователь уже существует";
//        }
//        return "ok";
//    }

    // поиск юзера по логину при авторизации
    public static UserModel searchUserByLogin(String loginJSON) {
        UserModel userModel = null;
        Gson gson = new Gson();
        LoginDTO loginDTO = gson.fromJson(loginJSON, LoginDTO.class);

        Map<String, UserModel> dataBase = new HashMap<>(DataBase.getInstance().returnBaseUser());

        for (Map.Entry entry : dataBase.entrySet()) {
            userModel = (UserModel) entry.getValue();
            if (userModel.getLogin().equals(loginDTO.getLogin()))
                return userModel;
        }
        return userModel;
    }
}
