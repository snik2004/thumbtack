package net.thumbtack.school.elections.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.CheckUser;
import net.thumbtack.school.elections.db.DataBase;
import net.thumbtack.school.elections.dto.request.LoginDTO;
import net.thumbtack.school.elections.dto.request.UsersDTO;
import net.thumbtack.school.elections.dto.response.ResponseLoginDTO;
import net.thumbtack.school.elections.model.UserModel;
import net.thumbtack.school.elections.exeption.ElectionsExeption;
import net.thumbtack.school.elections.model.VoteModel;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class Service {

    public void addUser(String json) throws ElectionsExeption {
        Gson gson = new Gson();
        UsersDTO userDTO = new UsersDTO();
        UserModel userModel = new UserModel();
        UserModel userModel1 = new UserModel();
        userDTO = gson.fromJson(json, UsersDTO.class);
        userModel1.setLogin(userDTO.getLogin());
        String responseFromUserLogin = CheckUser.checkAddUserLogin(gson.toJson(userModel1, UserModel.class));
        //userModel1 = gson.fromJson(userDTO.getCandidateUsername(), UsersDTO.class);

        //String responseFromUserLogin = gson.toJson(userModel1, UserModel.class);
        if (responseFromUserLogin.equals("ok") && DataBase.getInstance().returnBaseUser().get(userDTO.getLogin()) == null) {
            if (userDTO.getLogin().equals("против всех"))
                userModel.setToken("1");
            userModel.setLogin(userDTO.getLogin());
            userModel.setPassword(userDTO.getPassword());
            userModel.setFirstname(userDTO.getFirstname());
            userModel.setLastname(userDTO.getLastname());
            userModel.setMidlname(userDTO.getMidlname());
            userModel.setApartment(userDTO.getApartment());
            userModel.setHouse(userDTO.getHouse());
            userModel.setStreet(userDTO.getStreet());
            userModel.setCandidate(userDTO.getCandidate());
            userModel.setVoiter(userDTO.getVoiter());
            userModel.setNomination(userDTO.getNomination());
            DataBase.getInstance().addUser(userModel);
        }

    }


    // типа авторизация
    public String userLogin(String login) {
        Map<String, UserModel> dataBase = new HashMap<>(DataBase.getInstance().returnBaseUser());
        Gson gson = new Gson();
        String response;
        ResponseLoginDTO responseLoginDTO = new ResponseLoginDTO();
        UserModel userModel = new UserModel();
        LoginDTO loginDTO = new LoginDTO();
        UUID uuid = UUID.randomUUID();
        loginDTO = gson.fromJson(login, LoginDTO.class);
        if (CheckUser.searchUserByLogin(login) != null)
            for (Map.Entry entry : dataBase.entrySet()) {
                userModel = (UserModel) entry.getValue();
                if (loginDTO.getLogin().equals(userModel.getLogin())) {
                    if (loginDTO.getPassword().equals(userModel.getPassword())) {
                        userModel.setToken(uuid.toString());
                        //System.out.println("Авторизация пользователя " + userModel.getCandidateUsername() + " прошла успешно, токен пользователя: " + userModel.getCandidateUsername());
                        responseLoginDTO.setToken(userModel.getToken());
                        response = gson.toJson(responseLoginDTO, ResponseLoginDTO.class);
                        //System.out.println(response);
                        return response;
                    } else {
                        System.out.println("неправильный пароль");
                        return null;
                    }
                }
            }
        System.out.println("Пользователь с ником " + loginDTO.getLogin() + " не найден");
        return null;
    }

    //logout
    public void userLogout(String token) {
        ElectionService electionService = new ElectionService();
        UserModel userModel = new UserModel();
        Gson gson = new Gson();
        String response = null;
        ResponseLoginDTO responseLoginDTO = new ResponseLoginDTO();
        if (userModel != null) {
            userModel = searchUserByToken(token);
            electionService.noCandidate(gson.toJson(userModel));
            userModel.setToken(null);
            //System.out.println("Пользователь " + userModel.getCandidateUsername() + " успешно вышел из системы");
//            responseLoginDTO.setCandidateUsername(userModel.getCandidateUsername());
//            response = gson.toJson(responseLoginDTO, ResponseLoginDTO.class);
//            System.out.println(response);

            DataBase.getInstance().addUser(userModel);
        }
    }


    // поиск юзера по токену
    public UserModel searchUserByToken(String token) {
        Gson gson = new Gson();
        ResponseLoginDTO responseLoginDTO = gson.fromJson(token, ResponseLoginDTO.class);
        UserModel userModel = new UserModel();
        Map<String, UserModel> dataBase = new HashMap<>(DataBase.getInstance().returnBaseUser());
        for (Map.Entry entry : dataBase.entrySet()) {
            userModel = (UserModel) entry.getValue();
            if (userModel.getToken() != null && userModel.getToken().equals(responseLoginDTO.getToken()))
                return userModel;
        }
        userModel = null;
        return userModel;
    }

    // поиск юзера по логину при авторизации
    public UserModel searchUserByLogin(String login) {
        UserModel userModel = new UserModel();
        //LoginDTO loginDTO = new LoginDTO();
        Map<String, UserModel> dataBase = new HashMap<>(DataBase.getInstance().returnBaseUser());
        Gson gson = new Gson();
        UserModel userModelLogin = gson.fromJson(login, UserModel.class);
        for (Map.Entry entry : dataBase.entrySet()) {
            userModel = (UserModel) entry.getValue();
            if (userModel.getLogin().equals(userModelLogin.getLogin()))
                return userModel;
        }
        userModel = null;
        return userModel;
    }


    //чтение построчно с файла
    public void addUserInfoFromFile(String string) {
        //Map<String, UserModel> usersDB = new HashMap<>(DataBase.getInstance().returnBaseUser());
        DataBase dataBase = DataBase.getInstance();
        Gson gson = new Gson();
        UserModel userModel = gson.fromJson(string, UserModel.class);
        dataBase.addUser(userModel);
    }

    //получение всех юзеров для сохранения в файл
    public String getAllUserInfo() {
        Gson gson = new Gson();
        UserModel userModel = new UserModel();
        StringBuilder s = new StringBuilder();
        for (Map.Entry entry : DataBase.getInstance().returnBaseUser().entrySet()) {
            userModel = (UserModel) entry.getValue();
            s.append(gson.toJson(userModel));
            s.append("\n");
        }
        return s.toString();
    }

    //просмотр авторизованных пользователей для голосования
    public void onlineUser() {
        UserModel userModel = new UserModel();
        StringBuilder onlineUserList = new StringBuilder();
        for (Map.Entry entry : DataBase.getInstance().returnBaseUser().entrySet()) {
            userModel = (UserModel) entry.getValue();
            if (userModel.getToken() != null) {
                onlineUserList.append(userModel.getLogin());
                onlineUserList.append("\n");
            }
        }
        System.out.println("Следующие пользователи находятся в сети: \n" + onlineUserList);
    }

    //просмотр кандидатов пользователей для голосования
    public void candidateUser() {
        VoteModel voteModel = new VoteModel();
        StringBuilder candidateList = new StringBuilder();
        Map<String, VoteModel> voteModelMap = DataBase.getInstance().returnBaseVotes();
        for (Map.Entry entry : DataBase.getInstance().returnBaseVotes().entrySet()) {
            voteModel = (VoteModel) entry.getValue();
            if (voteModel.getCandidateUsename() != null) {
                candidateList.append(voteModel.getCandidateUsename());
                candidateList.append("\n");
            }
        }
        System.out.println("Следующие пользователи находятся в бюллетени: \n" + candidateList);
    }

}
