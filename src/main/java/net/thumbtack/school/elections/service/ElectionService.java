package net.thumbtack.school.elections.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.db.DataBase;
import net.thumbtack.school.elections.dto.request.VoteDTO;
import net.thumbtack.school.elections.dto.response.ResponseElectionDTO;
import net.thumbtack.school.elections.model.UserModel;
import net.thumbtack.school.elections.model.VoteModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ElectionService {
    //установка метки о выдвижении кандидата
    public String setCandidate(String userJSONCandidate) {
        Service service = new Service();
        UserModel userModel = service.searchUserByLogin(userJSONCandidate);
        userModel.setNomination(1);
        DataBase.getInstance().addUser(userModel);
        ResponseElectionDTO responseElectionDTO = new ResponseElectionDTO();
        responseElectionDTO.setCandidateUsername(userModel.getLogin());
        responseElectionDTO.setResponseMesage("Пользователю предложили выступить в роли кандидата");
        Gson gson = new Gson();
        String response = gson.toJson(responseElectionDTO);
        return response;
        //System.out.println("Пользователь " + userModel.getCandidateUsername() + " выдвинут на пост мера");
    }

    //снятие метки о выдвижении кандидата
    public String noCandidate(String userJSONCandidate) {
        Service service = new Service();
        UserModel userModel = service.searchUserByLogin(userJSONCandidate);
        userModel.setNomination(0);
        userModel.setCandidate(false);
        DataBase.getInstance().addUser(userModel);
        DataBase.getInstance().deleteCandidate(userModel.getLogin());
        //System.out.println("Пользователь " + userModel.getCandidateUsername() + " снялся со списка кандитатов");
        ResponseElectionDTO responseElectionDTO = new ResponseElectionDTO();
        responseElectionDTO.setCandidateUsername(userModel.getLogin());
        responseElectionDTO.setResponseMesage("пользователь больше не кандидат");
        Gson gson = new Gson();
        String response = gson.toJson(responseElectionDTO);
        return response;
    }

    // проверка есть ли метка на выдвижение
    public String checkNomination(String userJSONCandidate) {
        Service service = new Service();
        String response = "";
        UserModel userModel = service.searchUserByLogin(userJSONCandidate);
        VoteModel voteModel = new VoteModel();
        if (userModel.getNomination() == 1) {
            userModel.setCandidate(true);
            DataBase.getInstance().addUser(userModel);
            voteModel.setCandidateUsename(userModel.getLogin());
            voteModel.setCountvote(0);
            DataBase.getInstance().addVote(voteModel);
            //System.out.println("кандидат " + userModel.getCandidateUsername() + " подтвердил выдвижение");
            ResponseElectionDTO responseElectionDTO = new ResponseElectionDTO();
            responseElectionDTO.setCandidateUsername(userModel.getLogin());
            responseElectionDTO.setResponseMesage("выдвинут в кандидаты");
            Gson gson = new Gson();
            response = gson.toJson(responseElectionDTO);
        }

        return response;
    }

    //проверка есть ли метка о том что уже кандидат как самовыдвиженец
    public String checkStatusCandidate() {
        Map<String, UserModel> userModelMap = DataBase.getInstance().returnBaseUser();
        Map<String, VoteModel> voteModelMap = DataBase.getInstance().returnBaseVotes();
        UserModel userModel = new UserModel();
        //VoteModel voteModel = new VoteModel();
        for (Map.Entry entry : userModelMap.entrySet()) {
            userModel = (UserModel) entry.getValue();
            if (userModel.getCandidate() && userModel.getToken() != null) {
                VoteModel voteModel = new VoteModel();
                voteModel.setCandidateUsename(userModel.getLogin());
                voteModel.setToken(userModel.getToken());
                DataBase.getInstance().addVote(voteModel);
            }
        }
        ResponseElectionDTO responseElectionDTO = new ResponseElectionDTO();
        responseElectionDTO.setResponseMesage("все пользователи проверены");
        Gson gson = new Gson();
        String response = gson.toJson(responseElectionDTO);
        return response;
    }

    //голосование
    public String voting(String jsonVote) {
        Gson gson = new Gson();
        VoteDTO voteDTO = new VoteDTO();
        voteDTO = gson.fromJson(jsonVote, VoteDTO.class);
        ResponseElectionDTO responseElectionDTO = new ResponseElectionDTO();
        VoteModel voteModel = new VoteModel();
        Map<String, VoteModel> voteModelMap = DataBase.getInstance().returnBaseVotes();
        UserModel candidateUserModel = DataBase.getInstance().returnBaseUser().get(voteDTO.getCandidateUsername());
        UserModel voterUserModel = DataBase.getInstance().returnBaseUser().get(voteDTO.getVoteUsername());

        if (voterUserModel.getVoted() == 0 || candidateUserModel == null && voterUserModel.getVoted() == 0) {
            if (voteDTO.getCandidateUsername().equals("против всех")) {
                DataBase.getInstance().addAgainstVote();
                responseElectionDTO.setResponseMesage("голос против всех учтён " + candidateUserModel.getLogin());
                voterUserModel.setVoted(1);

            }

            if (DataBase.getInstance().returnBaseVotes().get(voteDTO.getCandidateUsername()) != null) {
                if (voteDTO.getCandidateUsername().equals(voteDTO.getVoteUsername())) {
                    responseElectionDTO.setResponseMesage("Нельзя голосовать за себя");
                    responseElectionDTO.setCandidateUsername(voteDTO.getCandidateUsername());
//                    System.out.println("Нельзя голосовать за себя " + voteDTO.getCandidateUsername());
                } else if (candidateUserModel.getToken() != null && !candidateUserModel.getToken().equals("")) {
                    voteModel.setToken(candidateUserModel.getToken());
                    voteModel.setCandidateUsename(voteDTO.getCandidateUsername());
                    if (voteModelMap.get(voteModel.getCandidateUsename()).getCountvote() == null) {
                        voteModel.setCountvote(1);
                        voterUserModel.setVoted(1);
                        responseElectionDTO.setResponseMesage("кандидат " + voteModel.getCandidateUsename() + " получил голос");
                        responseElectionDTO.setCandidateUsername(voteDTO.getCandidateUsername());
//                        System.out.println("кандидат " + voteModel.getCandidateUsename() + " получил голос от " + voteDTO.getVoteUsername());


                    } else {
                        voteModel.setCountvote(voteModelMap.get(voteModel.getCandidateUsename()).getCountvote() + 1);
//                        System.out.println("кандидат " + voteModel.getCandidateUsename() + " получил голос от " + voteDTO.getVoteUsername());
                        responseElectionDTO.setResponseMesage("кандидат " + voteModel.getCandidateUsename() + " получил голос");
                        responseElectionDTO.setCandidateUsername(voteDTO.getCandidateUsername());
                        voterUserModel.setVoted(1);
                    }
                } else {
//                    System.out.println("кандидата нет в списке " + voteDTO.getVoteUsername());
                    responseElectionDTO.setCandidateUsername(voteDTO.getCandidateUsername() + " нет в списке");
                }
                DataBase.getInstance().addVote(voteModel);
            } else {
                responseElectionDTO.setResponseMesage("кандидата нет в списке");
                responseElectionDTO.setCandidateUsername(voteDTO.getCandidateUsername());
//                System.out.println("кандидата " + voteDTO.getCandidateUsername() + " нет в списке " + voteDTO.getVoteUsername());
            }

        } else {
            responseElectionDTO.setResponseMesage("вы уже голосовали" + voteDTO.getVoteUsername());
//            System.out.println("вы уже голосовали " + voteDTO.getVoteUsername());
        }

        Gson gsonResponse = new Gson();
        String response = gsonResponse.toJson(responseElectionDTO);
        return response;
    }


    //просмотр и подсчёт голосов ГОТОВО
    public String voteListView() {
        VoteModel voteModel = new VoteModel();
        StringBuilder voteList = new StringBuilder();
        int count = 0;
        int countAagainstAll = 0;
        int countVS = 0;
        List<Integer> countTmp = new ArrayList<>();
        List<String> vinnerTmp = new ArrayList<>();
        String vinner = "";
        String vinnerVS = "";
        ResponseElectionDTO responseElectionDTO = new ResponseElectionDTO();
        String response = "";
        Gson gson = new Gson();

        //подсчёт голосов ГОТОВО
        for (Map.Entry entry : DataBase.getInstance().returnBaseVotes().entrySet()) {
            voteModel = (VoteModel) entry.getValue();
            if (voteModel.getToken() != null) {
                if (voteModel.getCandidateUsename() == null) {
                    DataBase.getInstance().addAgainstVote();
                    break;
                }
                if (voteModel.getCountvote() == null) {
                    voteModel.setCountvote(0);
                }
                voteList.append(voteModel.getCandidateUsename() + " " + voteModel.getCountvote());
                voteList.append("\n");
                if (voteModel.getCountvote() == null || voteModel.getCountvote() == 0) {
//                    System.out.println("за кандидата " + voteModel.getCandidateUsename() + " никто не проголосовал");
                }
                if (voteModel.getCountvote() > 0) {
                    if (voteModel.getCountvote() != null && voteModel.getCountvote() >= count) {
                        if (countTmp.size() >= 1 && countTmp.get(countTmp.size() - 1) < voteModel.getCountvote()) {
                            countTmp.clear();
                            vinnerTmp.clear();
                            count = voteModel.getCountvote();
                            vinner = voteModel.getCandidateUsename();
                            countTmp.add(count);
                            vinnerTmp.add(vinner);
                        } else if (voteModel.getCountvote() >= count) {
                            count = voteModel.getCountvote();
                            vinner = voteModel.getCandidateUsename();
                            countTmp.add(count);
                            vinnerTmp.add(vinner);
                        }
                    }
                }
            }
        }
        //вывод результатов ГОТОВО
        if (countTmp.size() > 1) {
//            System.out.println("\nРезультаты голосования: \n" + voteList);
//            System.out.println("Победитель не выявлен");
//            System.out.println("кандидаты с одинаковым количеством голосов:");
            for (int i = 0; i < countTmp.size(); i++) {
                responseElectionDTO.setCandidateUsername(voteModel.getCandidateUsename());
                responseElectionDTO.setResponseMesage("Абсолютный победитель не выявлен:\n" + vinnerTmp.get(i) +
                        " за него проголосовало " + countTmp.get(i) + " человек(а). ");
                gson = new Gson();
                response = gson.toJson(responseElectionDTO);
//                System.out.println(vinnerTmp.get(i) + " за него проголосовало " + countTmp.get(i) + " человек(а)");
            }
        } else if (count > DataBase.getInstance().returnCountAgianst()) {
            responseElectionDTO.setCandidateUsername(voteModel.getCandidateUsename());
            responseElectionDTO.setResponseMesage("Абсолютный победитель " + vinner + " за него проголосовало " + count + " человек(а)");
            gson = new Gson();
            response = gson.toJson(responseElectionDTO);
//            System.out.println("Результаты голосования: \n" + voteList);
//            System.out.println("Абсолютный победитель " + vinner + " за него проголосовало " + count + " человек(а)");
        } else if (count == DataBase.getInstance().returnCountAgianst()) {
//            System.out.println("Никто не победил " + DataBase.getInstance().returnCountAgianst());
            responseElectionDTO.setResponseMesage("Никто не победил");
            response = gson.toJson(responseElectionDTO);
        } else if (count < DataBase.getInstance().returnCountAgianst()) {
//            System.out.println("Никто не победил, против всех набрало больше всего голосов" + DataBase.getInstance().returnCountAgianst());
            responseElectionDTO.setResponseMesage("Никто не победил");
            response = gson.toJson(responseElectionDTO);
        }
        return response;
    }

}
