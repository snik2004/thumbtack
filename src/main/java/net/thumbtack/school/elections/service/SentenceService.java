package net.thumbtack.school.elections.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.db.DataBase;
import net.thumbtack.school.elections.dto.request.SentenceAddDTO;
import net.thumbtack.school.elections.dto.request.SentenceAddRateDTO;
import net.thumbtack.school.elections.dto.request.SentenceCandidateAddDTO;
import net.thumbtack.school.elections.dto.response.ResponceOkDTO;
import net.thumbtack.school.elections.dto.response.ResponseErrorDTO;
import net.thumbtack.school.elections.model.*;
import net.thumbtack.school.elections.server.Server;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SentenceService {

    //добавить предложение
    public String addSentence(String json) {
        String responceJSON;
        Gson gson = new Gson();
        SentenceAddDTO sentenceAddDTO = new SentenceAddDTO();
        sentenceAddDTO = gson.fromJson(json, SentenceAddDTO.class);
        Map<Integer, SentenceModel> map = DataBase.getInstance().returnBaseRateSentence();
        RateModel rateModel = new RateModel();
        List sentence = DataBase.getInstance().returnBaseSentence();
        List<RateModel> rateModelList = new ArrayList<>();
        int trigger = 0;
        for (int i = 0; i < sentence.size(); i++) {
            if (sentence.get(i).equals(sentenceAddDTO.getSentence())) {
                trigger++;
            }
        }
        if (trigger == 0) {
            SentenceModel sentenceModel = new SentenceModel();
            sentenceModel.setSentence(sentenceAddDTO.getSentence());
            sentenceModel.setAutor(sentenceAddDTO.getAutor());
            rateModel.setRateAutor(sentenceAddDTO.getAutor());
            if (sentence.size() == 0)
                rateModel.setSentencePosition(0);
            else {
                rateModel.setSentencePosition(sentence.size());
            }
            rateModel.setRate(5);
            sentenceModel.setId(rateModel.getSentencePosition());
            sentenceModel.getRateModel().add(rateModel);
            DataBase.getInstance().addSentence(sentenceModel);

            ResponceOkDTO responceOkDTO = new ResponceOkDTO();
            responceOkDTO.setOk("ok");
            responceJSON = gson.toJson(responceOkDTO, ResponceOkDTO.class);

        } else {
            ResponseErrorDTO responseErrorDTO = new ResponseErrorDTO();
            responseErrorDTO.setError("Error add sentence");
            responseErrorDTO.setErrormessage("Предложение \"" + sentenceAddDTO.getSentence() + "\" уже существует, поставьте ему оценку.");
            System.out.println("Предложение \"" + sentenceAddDTO.getSentence() + "\" уже существует, поставьте ему оценку.");
            responceJSON = gson.toJson(responseErrorDTO, ResponseErrorDTO.class);

        }
//        sentenceModel.setRateModel(rateModelList);


        //оценка
//        if (sentenceAddDTO.showAllSentence() != null)
//            sentence.add(sentenceAddDTO.showAllSentence());
//        rateModel.setSentencePosition(sentence.size());
//        sentenceAddDTO = gson.fromJson(json, SentenceAddDTO.class);
//        rateModel.setRateAutor(sentenceAddDTO.getAutor());
//        if (sentenceAddDTO.getCandidate() != null)
//            rateModel.setIdCandidateProgram(sentenceAddDTO.getCandidate());


//        sentenceModel.setAutor(sentenceAddDTO.getAutor());
//        sentenceModel.setCandidate(sentenceAddDTO.getCandidate());
//        sentenceModel.setRateModel(sentenceAddDTO.getRateModel());
//        sentenceModel.setSentence(sentenceAddDTO.showAllSentence());
        return responceJSON;
    }

    //  добавить оценку
    public String addRate(String json) {
        String responceJSON;

        Gson gson = new Gson();
        SentenceAddRateDTO sentenceAddRateDTO = gson.fromJson(json, SentenceAddRateDTO.class);
        SentenceModel sentenceModel = new SentenceModel();
        List sentence = DataBase.getInstance().returnBaseSentence();
        RateModel rateModel = new RateModel();
        Map<Integer, SentenceModel> map = DataBase.getInstance().returnBaseRateSentence();
        int trigger = 0;
        for (int i = 0; i < sentence.size(); i++) {
            if (sentence.get(i).equals(sentenceAddRateDTO.getSentence())) {
                sentenceModel.setId(i);
                trigger++;
            }
        }
        if (trigger != 0) {
            sentenceModel = map.get(sentenceModel.getId());
            rateModel.setRateAutor(sentenceAddRateDTO.getUser());
            rateModel.setRate(sentenceAddRateDTO.getRate());
            rateModel.setSentencePosition(sentenceModel.getId());
            sentenceModel.getRateModel().add(rateModel);
            DataBase.getInstance().addRateSentence(sentenceModel);
            ResponceOkDTO responceOkDTO = new ResponceOkDTO();
            responceOkDTO.setOk("ok");
            responceJSON = gson.toJson(responceOkDTO, ResponceOkDTO.class);
        } else {
            ResponseErrorDTO responseErrorDTO = new ResponseErrorDTO();
            responseErrorDTO.setError("Error add rate");
            responseErrorDTO.setErrormessage("Предложение \"" + sentenceAddRateDTO.getSentence() + "\" не найдено.");
            System.out.println("Предложение \"" + sentenceAddRateDTO.getSentence() + "\" не найдено.");
            responceJSON = gson.toJson(responseErrorDTO, ResponseErrorDTO.class);

        }
        return responceJSON;
    }

    //поиск предложений кандидата
    public void searchSentence(String json) {
        Map<String, UserModel> userModelMap = DataBase.getInstance().returnBaseUser();
        List list = DataBase.getInstance().returnBaseSentence();
        Map<String, ProgramModel> programModelMap = DataBase.getInstance().returnBaseProgram();
        ProgramModel programModel = new ProgramModel();
        Gson gson = new Gson();
        int count = 0;
        SentenceCandidateAddDTO sentenceCandidateAddDTO = gson.fromJson(json, SentenceCandidateAddDTO.class);
        List<RateModel> rateModel = new ArrayList<>();

        if (userModelMap.get(sentenceCandidateAddDTO.getCandidate()) != null) {
            for (Map.Entry entry : programModelMap.entrySet()) {
                if (entry.getKey().equals(sentenceCandidateAddDTO.getCandidate())) {
                    programModel = (ProgramModel) entry.getValue();
                    System.out.println("\nПредложения кандидата "+ programModel.getCandidate()+":\n");
                }
                for (int i = 0; i < programModel.getSentenceModelList().size(); i++) {
                    rateModel = programModel.getSentenceModelList().get(i).getRateModel();
                    if (sentenceCandidateAddDTO.getSentence().equals("all")) {
                        for (RateModel rate : rateModel) {
                            count += rate.getRate();
                        }
                        count = count / rateModel.size();
                        System.out.println("предложение кандидата " + programModel.getCandidate() + ": " + programModel.getSentenceModelList().get(i).getSentence() + " со средней оценкой " + count);
                        count = 0;
                    } else if (programModel.getSentenceModelList().get(i).getSentence().equals(sentenceCandidateAddDTO.getSentence())) {
                        for (RateModel rate : rateModel) {
                            count += rate.getRate();
                        }
                        count = count / rateModel.size();
                        System.out.println("предложение кандидата " + programModel.getCandidate() + ": " + sentenceCandidateAddDTO.getSentence() + " со средней оценкой " + count);
                    }
                }
            }
        }
    }

    //добавление предложения кандидату
    public ResponseErrorDTO addSentenceCandidate(String json) {
        Gson gson = new Gson();
        Map<String, ProgramModel> programModelMap = DataBase.getInstance().returnBaseProgram();
        SentenceCandidateAddDTO sentenceCandidateAddDTO = gson.fromJson(json, SentenceCandidateAddDTO.class);
        List sentenceList = DataBase.getInstance().returnBaseSentence();
        SentenceModel sentenceModel = new SentenceModel();
        ProgramModel programModel = programModelMap.get(sentenceCandidateAddDTO.getCandidate());
        int count = 0;
        //???????
        if (sentenceCandidateAddDTO.getCandidate().equals(DataBase.getInstance().returnBaseVotes())) {

        }
        if (programModel == null) {
            programModel = new ProgramModel();
        }
        for (int i = 0; i < programModel.getSentenceModelList().size(); i++) {
            //String s = programModel.getSentenceModelList().get(i).getSentence();
            if (programModel.getSentenceModelList().get(i).getSentence().equals(sentenceCandidateAddDTO.getSentence())) {
                System.out.println("Это предложение уже в программе у кандидата\n");
                ResponseErrorDTO responseErrorDTO = new ResponseErrorDTO();
                responseErrorDTO.setError("error");
                responseErrorDTO.setErrormessage("это предложение уже в программе у кандидата");
                return responseErrorDTO;
            }
        }
        for (int i = 0; i < sentenceList.size(); i++) {

            if (sentenceList.get(i).equals(sentenceCandidateAddDTO.getSentence())) {
                sentenceModel = DataBase.getInstance().returnBaseRateSentence().get(i);
                programModel.setCandidate(sentenceCandidateAddDTO.getCandidate());
                count = 1;
//                if (programModelMap.size() == 0) {
//                    programModel.setId(0);
//                }else {
//                    programModel.setId(programModelMap.size()+1);
//                }
            }
        }
        if (count == 0) {
            System.out.println("такого предложения никто не выдвигал");
        } else
            programModel.getSentenceModelList().add(sentenceModel);
        DataBase.getInstance().addProgramSentence(programModel);
        return null;
    }


    //просмотр всех всех предложений наверное не нужно
    public void showAllSentence() {
        SentenceModel sentenceModel = new SentenceModel();
        List list = DataBase.getInstance().returnBaseSentence();
        Map<Integer, SentenceModel> sentenceRateMap = DataBase.getInstance().returnBaseRateSentence();
        List<RateModel> rateModel = new ArrayList<>();
        int count = 0;
        for (Map.Entry entry : sentenceRateMap.entrySet()) {

            sentenceModel = (SentenceModel) entry.getValue();
            rateModel = sentenceModel.getRateModel();
            //count += rateModel.getRate();
        }
        for (int i = 0; i < list.size(); i++) {
            sentenceModel = sentenceRateMap.get(i);
            rateModel = sentenceModel.getRateModel();
            for (RateModel rate : rateModel) {
                count += rate.getRate();
            }
            count = count / rateModel.size();
            System.out.println("предложения " + sentenceModel.getSentence() + " от пользователя " +
                    sentenceModel.getAutor() + " со средней оценкой " + count);
            sentenceModel = sentenceRateMap.get(list.get(i));
            count = 0;
        }
    }

    //получение предложений выборочно
    public String showSentence() {
        Gson gson = new Gson();
        SentenceModel sentenceModel = new SentenceModel();
        StringBuilder s = new StringBuilder();
        for (Map.Entry entry : DataBase.getInstance().returnBaseRateSentence().entrySet()) {
            sentenceModel = (SentenceModel) entry.getValue();
            s.append(gson.toJson(sentenceModel));
            s.append("\n");
        }
        //System.out.println(s);
        return s.toString();
    }


}
