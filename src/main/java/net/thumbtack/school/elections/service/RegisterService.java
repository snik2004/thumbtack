package net.thumbtack.school.elections.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.CheckUser;
import net.thumbtack.school.elections.db.DataBase;
import net.thumbtack.school.elections.dto.request.UsersDTO;
import net.thumbtack.school.elections.dto.response.ResponseErrorLoginDTO;
import net.thumbtack.school.elections.exeption.ElectionsExeption;
import net.thumbtack.school.elections.model.UserModel;

import java.util.HashMap;
import java.util.Map;

public class RegisterService {

    public static String registerUser(String json) throws ElectionsExeption {
        Gson gson = new Gson();
        UsersDTO userDTO = gson.fromJson(json, UsersDTO.class);
        Service service = new Service();
        Map<String, UserModel> checkUserDataBase = new HashMap<>(DataBase.getInstance().returnBaseUser());
        ResponseErrorLoginDTO responseErrorLoginDTO = new ResponseErrorLoginDTO();
        UserModel userDTOSearchLogin = new UserModel();

        json = gson.toJson(userDTO);
        //System.out.println("конвертирование dto в стринг " + json);
        String response = CheckUser.checkUserJSON(userDTO);
        if (response.equals("ok")) {
//            if (userDTO.getLogin()=="против всех"){
//                service.addUser(json);
//            }
            for (Map.Entry entry : checkUserDataBase.entrySet()) {
                userDTOSearchLogin = (UserModel) entry.getValue();
                if (userDTO.getLogin().equals(userDTOSearchLogin.getLogin())) {
                    //System.out.println("Пользователь под ником " + userDTO.getCandidateUsername() + " уже зарегистрирован");
                    responseErrorLoginDTO.setError("Ник занят");
                    json = gson.toJson(responseErrorLoginDTO, ResponseErrorLoginDTO.class);
                    return response;
                }
            }
            service.addUser(json);
        }
        return response;
    }
}
