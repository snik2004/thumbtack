package net.thumbtack.school.elections.service;

import com.google.gson.Gson;
import net.thumbtack.school.elections.model.SentenceModel;
import net.thumbtack.school.elections.model.UserModel;
import net.thumbtack.school.elections.model.VoteModel;

public class ConvertJSON {
    String responseConvert;
    public ConvertJSON() {
    }

    public String convertUserModelToJson(UserModel userModel) {
        Gson gson = new Gson();
        responseConvert = gson.toJson(userModel);
        return responseConvert;
    }

    public String convertSentenceModelToJson(SentenceModel sentenceModel) {
        Gson gson = new Gson();
        responseConvert = gson.toJson(sentenceModel);
        return responseConvert;
    }

    public String convertVoteModelToJson(VoteModel voteModel) {
        Gson gson = new Gson();
        responseConvert = gson.toJson(voteModel);
        return responseConvert;
    }


}
