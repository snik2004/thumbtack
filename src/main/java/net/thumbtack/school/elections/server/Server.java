package net.thumbtack.school.elections.server;

import net.thumbtack.school.elections.CheckUser;
import net.thumbtack.school.elections.exeption.ElectionsExeption;
import net.thumbtack.school.elections.service.RegisterService;
import net.thumbtack.school.elections.service.Service;

import java.io.*;


public class Server {
    public void main(String[] args) throws IOException, ElectionsExeption {
        startServer("1.txt");
//            DataBase db = DataBase.getInstance();
//         Gson gson = new Gson();
//        String string = "{ \"user\": { \"fullname\": { \"firstname\": \"nikita\", \"midlname\": \"pavlovich\", \"lastname\": \"garbar\" }," +
//                " \"address\": { \"street\": \"street1\", \"house\": 32, \"apartment\": 32 }, \"login\": \"snik2004\", \"password\": \"pass\", \"voiter\":true, \"candidate\":false } }";
//        String string2 = "{ \"user\": { \"fullname\": { \"firstname\": \"sergey\", \"midlname\": \"pavlovich\", \"lastname\": \"garbar\" }," +
//                " \"address\": { \"street\": \"street1\", \"house\": 32, \"apartment\": 32 }, \"login\": \"user2\", \"password\": \"password1\", \"voiter\":true, \"candidate\":false } }";
//        String string3 = "{ \"user\": { \"fullname\": { \"firstname\": \"sergey\", \"midlname\": \"pavlovich\", \"lastname\": \"garbar\" }," +
//                " \"address\": { \"street\": \"street1\", \"house\": 32, \"apartment\": 32 }, \"login\": \"user3\", \"password\": \"password1\", \"voiter\":true, \"candidate\":false } }";
//        UserDTO userDTO = gson.fromJson(string, UserDTO.class);
//        net.thumbtack.school.elections.dto.request.UserDTO userDTO1 = gson.fromJson(string, net.thumbtack.school.elections.dto.request.UserDTO.class);
//
//        System.out.println(userDTO1.getUser().getCandidateUsername());
//
//        db.addUser(string);
//        db.addUser(string2);
//       db.addUser(string3);
//
//        System.out.println(registerUser(string));

        System.out.println("privet");
        stopServer("1.txt");
    }

    public String logIn(String loginJSON) {
        Service service = new Service();
        String response;
        if (loginJSON!=null) {
            response = service.userLogin(loginJSON);
            if (response == null) {
                return null;
            } else {
                return response;
            }
        } else{
            System.out.println("какая то херня");
            return "error";}

    }

    public void logOut(String string) {
        Service service = new Service();
        service.userLogout(string);
        //System.out.println("logout completed");
    }

    public void startServer(String savedDataFileName) throws IOException, ElectionsExeption {
        String jsonAgainst = "{\"login\":\"против всех\",\"token\":\"1\",\"candidate\": true }";
        RegisterService.registerUser(jsonAgainst);
        if (savedDataFileName != null) {
            Service service = new Service();
            try (BufferedReader bufferedReader = new BufferedReader(new FileReader(savedDataFileName))) {
                String s;
                while ((s = bufferedReader.readLine()) != null) {
                    //System.out.println(s);
                    service.addUserInfoFromFile(s);
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        File file = new File(savedDataFileName);
        file.delete();
    }

    public static void stopServer(String saveDataFileName) throws IOException {
        Service service = new Service();

        if (saveDataFileName != null) {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(saveDataFileName));

            bufferedWriter.write(service.getAllUserInfo());
            bufferedWriter.flush();
        }
    }

}
